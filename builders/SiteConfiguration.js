const livePersonHelper = require("../helpers/LivePersonHelper").LivePersonHelper;
const SmartConnectorConfiguration = require('../model/SmartConnectorConfiguration').SmartConnectorConfiguration;
const requestHelper = require('../helpers/RequestHelper').RequestHelper;

class SiteConfiguration{
    
    configure(site){
        
        this.siteId = site.id;
        this.serviceId = site.serviceId;

        return this;
    };

    grantLPAjspAdminPermissions(){

       this.promise = livePersonHelper.grantLPAjspAdminPermissions(this.siteId);

       return this;
    };

    logIntoLiveEngage(){
        
        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.logIntoLiveEngage(siteId))
                                       .then(headers => this.headers = headers)
                
        }else{
            console.log(this.promise)
        }

        return this;
    };

    prepareSmartConnectorWidget(){

        let siteId = this.siteId;

        if(this.promise){ 
            this.promise = this.promise.then(() => livePersonHelper.prepareSmartConnectorWidget(siteId))
                                       .then( ([smartConnectorWidget, subDomain]) => {
                                                this.smartConnectorWidget = smartConnectorWidget;
                                                this.subDomain = subDomain;   
                                        })
                                        .catch(error =>{
                                            console.log(error);
                                        })
        }else{
            console.log(this.promise)
        }

        return this;
    };

    installSmartConnectorWidget(){

        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.installSmartConnectorWidget(siteId, this.smartConnectorWidget, this.headers))
                                       .then( smartConnectorWidgetInstallationKeys => this.smartConnectorWidgetInstallationKeys = smartConnectorWidgetInstallationKeys);

        }else{
            console.log(this.promise)
        }

        return this;
    };

    createMavenSkill() {

        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.createMavenSkill(siteId,this.headers, this.subDomain))
                                       .then( agentSmartConnectorWidget => {
                                        this.agentSmartConnectorWidget = agentSmartConnectorWidget
                                        this.skillId = this.agentSmartConnectorWidget.skillIds[0];
                                       });
        }

        return this;
    };

    createLiveEngageSmartConnectorWidget(){

        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.createLiveEngageSmartConnectorWidget(siteId,this.agentSmartConnectorWidget, this.headers))
                                       .then( agentSmartConnectorWidget => this.agentSmartConnectorWidget = agentSmartConnectorWidget)
        }

        return this;
    };

    createLiveEngageSmartConnectorApi(){

        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.createLiveEngageSmartConnectorApi(siteId,this.headers))
                                       .then( smartConnectorApi => this.smartConnectorApi = smartConnectorApi)
        }

        return this;
    };


    setSmartConnectorBotUserProfiles(){

        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.setSmartConnectorBotUserProfiles(siteId,this.headers))
                                       .then( ([agentManagerProfileId, agentProfileId]) =>{
                                                this.agentManagerProfileId = agentManagerProfileId;
                                                this.agentProfileId = agentProfileId 
                                       })

        }

        return this;
    }
    getSiteSkillRules() {
        
        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.getSiteSkillRules(siteId, this.serviceId, this.skillId, this.headers))
                                       .then( ([revisionNumber, skillRules]) => {
                                               this.skillRules = skillRules;   
                                               this.revisionNumber = revisionNumber;
                                        });
        }

        return this;
    };

    enableACFeatures(){

        if(this.promise){
            
            this.promise = this.promise.then(() => Object.assign({"If-Match": 0}, Object.assign({}, this.headers)))
                                       .then((headers) => livePersonHelper.enableACFeatures(this.siteId, headers))
                                       .then( skillRules => this.skillRules = skillRules);
        }

        return this;
    };

    createSmartConnectorUser() {

        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => requestHelper.getLEUserRequest(this.smartConnectorApi.keyId, this.skillId, this.agentProfileId ,this.agentManagerProfileId))
                                       .then((smartConnectorBotUser) => livePersonHelper.createSmartConnectorUser(siteId, smartConnectorBotUser, headers))
                                       .then( skillRules => this.skillRules = skillRules);
        }

        return this;
    }
    updateSiteSkillRules(){

        let siteId = this.siteId;

        if(this.promise){
            
            this.promise = this.promise.then(() => Object.assign({},this.headers))
                                       .then((headers) => livePersonHelper.updateSiteSkillRules(siteId,this.skillRules,this.revisionNumber, headers))
                                      
        }

        return this;
    }
    saveConfiguration(){

        if(this.promise){
           return this.promise
                     .then(() => {
                        
                        console.log("Returning SmartConnector configuration on siteId: "+this.siteId);
                        //@TODO not finished yet
                    let scConfig = new SmartConnectorConfiguration(
                            this.siteId,
                            this.smartConnectorWidgetInstallationKeys.client_id,
                            this.smartConnectorWidgetInstallationKeys.client_secret,
                            "mavenbot",
                            this.smartConnectorApi.keyId,
                            this.smartConnectorApi.appSecret,
                            this.smartConnectorApi.token,
                            this.smartConnectorApi.tokenSecret
                        );

                        console.log(scConfig);
                  })
        }
       
    }
}

module.exports = {
    siteConfiguration: new SiteConfiguration()
};