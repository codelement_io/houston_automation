require('dotenv').config()
const puppeteer = require('puppeteer');
const axios = require('axios');
const uHelper = require("./UrlHelper").UrlHelper;
const service = require("../resources/services");
const User = require("../model/User").User;
const requestHelper = require("./RequestHelper").RequestHelper;

const LivePersonHelper = {

    logIntoLiveEngage: (siteId) => {

     return axios.get(uHelper.getDomainUrl(siteId, service.agentVep))
             .then( response => {
                let domain = response.data.baseURI;
          
               return axios.post(uHelper.getLoginUrl(domain, siteId), new User(process.env.LPA_USER_NAME, process.env.PASSWORD))
                          .then( response => {

                            console.log("Logged into LiveEngage - siteId: "+siteId);

                            token = response.data.bearer;
                            userId = response.data.config.userId;
                            headers = requestHelper.getAuthHeaderBySite(token, userId, siteId, '-1');

                            return headers;

                         })
                         .catch( error => {
                             console.log(error);
                         })
             })
             
    },

    prepareSmartConnectorWidget: (siteId) => {

        console.log("Preparing SmartConnector widget on siteId: "+siteId);

        return axios.get(uHelper.getDomainUrl(siteId, service.sentinel))
                    .then( response => {

                      let subDomain = response.data.baseURI.split(".")[0];
                      let SCWidget = requestHelper.getConnectorConfigBySite(siteId,subDomain);
                      
                      return [SCWidget, subDomain];
                    })
                    .catch( error => {
                        return error;
                    });
              
      },

      installSmartConnectorWidget: (siteId, smartConnectorWidget, headers) => {
      
       
        let installedSmartConnectorWidget;

        return  axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                    .then( response => { 
                        return response.data.baseURI;
                    })
                    .then(readWriteDomain => {
                        domain = readWriteDomain;
                        return axios.get(uHelper.getHoustonNewAppManagementUrl(siteId,readWriteDomain),{headers: headers})
                    })
                    .then(installations => {
                    
                        return installations.data.find(installation => installation.client_name == "SmartConnect Agent Widget")
                    })
                    .then( smartConnectorWidgetIsInstalled => {

                        if(smartConnectorWidgetIsInstalled){
                            console.log("Existing SmartConnect Agent Widget was found on siteId ["+siteId+"] with Id: "+smartConnectorWidgetIsInstalled.id);
                            installedSmartConnectorWidget = smartConnectorWidgetIsInstalled;
                            return installedSmartConnectorWidget;
                        }else{

                          console.log("Installing on Houston SmartConnector widget on siteId: "+siteId);

                          return axios.post(uHelper.getHoustonNewAppManagementUrl(siteId,domain),smartConnectorWidget,{headers: headers})
                                      .then( smartConnectotWidgetInstallation =>{
                                            installedSmartConnectorWidget = smartConnectotWidgetInstallation.data;
                                            return installedSmartConnectorWidget
                                       })
                                        .catch( error => {
                                            console.log(error);
                                            return error;
                                        })
                        }
                                     
                    })
            
        },
    grantLPAjspAdminPermissions: (siteId) => {

      return  axios.get(uHelper.getDomainUrl(siteId, service.openPlatform))
                    .then( response => {
                        return response.data.baseURI;
                    })
                    .then( domain => {

                        let jspAdminUrl    = uHelper.getJspAdminUrl(siteId, domain);
                        let userName       = process.env.USER_NAME;
                        let passWord       = process.env.PASSWORD;
                            
                        return  LivePersonHelper.authenticateJspAdmin(siteId, jspAdminUrl, userName, passWord);

                        //  return siteId;
                    })
                    .catch(error =>{
                        console.log("error",error)
                    })
        
     },

     createMavenSkill: (siteId, headers, subDomain) => {

       let skill = requestHelper.getSkillRequest();

       return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                   .then( response => { 
                        return response.data.baseURI;
                   })
                   .then(readWriteDomain => {
                        domain = readWriteDomain;
                        return axios.get(uHelper.getAllSkillsUrl(readWriteDomain, siteId),{headers: headers})
                    })
                    .then(skills => {
                        
                        return skills.data.find(skill => skill.name == "maven")
                    })
                    .then( skillExists => {
                     
                        if(skillExists){
                            console.log("Existing Maven Skill was found on siteId ["+siteId+"] with Id: "+skillExists.id);
                            skill = skillExists;
                            return requestHelper.getWidgetConfigBySite(skill.id,subDomain);
                        }else{
                            return axios.post(uHelper.getLESkillsUrl(domain, siteId),skill,{headers: headers})
                                        .then( response => {
                                            skill = response.data;
                                            return requestHelper.getWidgetConfigBySite(skill.id,subDomain);
                                        })
                                        .catch( error => {
                                            console.log("error",error);
                                            return error;
                                        })
                        }
                        
                    })
                    .catch( error => {
                        console.log("error",error);
                        return error;
                    })
                                          
     },
     getSiteSkillRules: (siteId, serviceId, skillId, headers) => {
                
        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadOnly))
                    .then( response => {
                        return  axios.get(uHelper.getAllSkillRulesUrl(siteId, response.data.baseURI),{headers: headers})           
                    })
                    .then( response => {

                        console.log("Getting Skill Rules from siteId: "+siteId);
        
                         revisionNumber = response.headers['ac-revision'];
                         skillRules = response.data;                                                                              
                         rules = skillRules.propertyValue.value.rules;

                        if(rules && rules.length > 0){

                            skillRule = rules.find(rule => rule.description == "Vodafone RCS");
                            if(skillRule){
                                console.log("Existing Maven Skill Rule was found on siteId ["+siteId+"] with orderId: "+skillRule.orderId);
                            }else{
                                orderId = Math.max.apply(Math, rules.map(function(rule) { return rule.orderId; }));
                                newRule = requestHelper.getSingleSkillRuleRequest(skillId, (orderId + 1), serviceId);
                                rules.push(newRule);
                            }
                            console.log("if",[revisionNumber, skillRules.propertyValue.value])
                        }else{
                            skillRules = requestHelper.getFullSkillRuleRequest(skillId, 1, serviceId);
                            console.log("else",[revisionNumber, skillRules]);
                          
                        }
                       
                        return [revisionNumber, skillRules];
                    })
                    .catch( error => {
                        
                        console.log("error",error);
                        return error;
                    })
    },

    setSmartConnectorBotUserProfiles: (siteId,headers) => {

        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                    .then( response => {

                        let readWriteDomain = response.data.baseURI; 
    
                    return axios.get(uHelper.getLEProfilesUrl(siteId,readWriteDomain),{headers: headers})
                                .then( response => {

                                    console.log("Getting Agent/AgentManager profiles Id on siteId: "+siteId);

                                    let profiles = response.data;

                                   /**
                                    * @TODO fetch profiles by LE roles
                                    LivePersonHelper.getAgentManagerAndAgentProfileId(profiles,siteId,readWriteDomain)      

                                    */  
                                    // This is just a temporary solution profiles might have any names it's necessary to find by profile role
                                    let managerRoleId = profiles.find(profile => (profile.name == "Agent Manager" || profile.name == "AgentManager")).id;
                                    let agentRoleId = profiles.find(profile => profile.name == "Agent").id;

                                    return [managerRoleId, agentRoleId];

                                })          
                    })
                    .catch( error => {
                       console.log("error",error);
                       return error;
                   })

                   
    },
    updateSiteSkillRules: (siteId, skillRules, revisionNumber,headers) => {
        
        console.log("Updating Site Skill Rules on siteId: "+siteId);

        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                    .then( response => { 

                        let readWriteDomain = response.data.baseURI;
                        headers["If-Match"]= revisionNumber;
                        console.log("skillRules",skillRules);
                        console.log("Skill Rules Headers",headers);
                        return axios.put(uHelper.getSkillRuleUrl(siteId,readWriteDomain),skillRules,{headers: headers})
                                    
                    })
                    .catch( error => {
                        console.log(error);
                        return error;
                    })
    },

    enableACFeatures: (siteId, headers) => {

    console.log("Enabling ACfeatures on siteId: "+siteId);

     return  axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
             .then( response => {

                userDomain = response.data.baseURI;
                botTypeRequest = requestHelper.getBotTypeACFeature(siteId);
                headers["X-HTTP-Method-Override"] = "PUT";

                    return axios.post(uHelper.getLEBotTypeAcFeatureURl(siteId,userDomain),botTypeRequest,{headers: headers})
                    
                })
                .catch( error => {
                    console.log("error",error);
                })
    },
    createLiveEngageSmartConnectorWidget: (siteId, liveEngageSmartConnectorWidget,headers) => {
        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                    .then( response => { 
                        return response.data.baseURI;
                    })
                    .then(readWriteDomain => {
                        domain = readWriteDomain;
                        return axios.get(uHelper.getAllLEWidgetsUrl(readWriteDomain, siteId),{headers: headers})
                    })
                    .then( widgets => {
                      
                        return widgets.data.find(widget => widget.name == "SmartConnect Widget")
                    })
                    .then(widgetExists => {

                        if(widgetExists){
                            foundWidget = widgetExists;
                            console.log("Existing SmartConnector Widget was found on siteId ["+siteId+"] with id: "+foundWidget.id);
                            return foundWidget;
                        }else{

                            console.log("Creating LiveEngage SmartConnector on siteId: "+siteId);

                            return  axios.post(uHelper.getLECreateWidgetUrl(siteId,domain),liveEngageSmartConnectorWidget,{headers: headers}); 
                        }
                                
                    })
                    .catch( error => {
                        console.log("error",error);
                        return error;
                    })
                  

    },

    createLiveEngageSmartConnectorApi:  (siteId, headers) => {

        return axios.get(uHelper.getDomainUrl(siteId, service.appKeyManagement))
                    .then( response => {
                        return response.data.baseURI;
                    })
                    .then( appKeyDomain => {
                         
                        domain = appKeyDomain;
                        return axios.get(uHelper.getAppKeyManagementUrl(siteId, appKeyDomain),{headers: headers});                 
                    })
                    .then(liveEngageAPIs => {
                    
                        return liveEngageAPIs.data.find(api => api.appName == "SmartConnect Bot");
                    })
                    .then(smartConnectApiExists => {
               
                        if(smartConnectApiExists){
                             smartConnectorAPI = smartConnectApiExists;
                            console.log("Existing SmartConnector API was found on siteId ["+siteId+"] with keyId: "+smartConnectorAPI.keyId);
            
                            return smartConnectorAPI;
                        }else{
                            console.log("Creating SmartConnector API on siteId: "+siteId);

                            let apiKeyRequest = requestHelper.getLEApiRequest();
                            return axios.post(uHelper.getAppKeyManagementUrl(siteId, domain),apiKeyRequest,{headers: headers})
                                        .then( response => {
                                            return response.data;
                                        })
                        
                        }
                    })
                    .catch( error => {

                        console.log("error",error);
                        return error;
                    })
    },

    createSmartConnectorUser: (siteId, smartConnectorBotUser, headers) => {
      
        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                    .then( response => { 
    
                        readWriteDomain = response.data.baseURI;
                        
                        console.log("Creating SmartConnector User on siteId: "+siteId);
                        
                            return  axios.post(uHelper.getLECreateUserUrl(siteId,readWriteDomain),smartConnectorBotUser,{headers: headers})
                                         .then( response => {
                                           
                                            return response.data;
                                         })
                                         .catch(error => {
                                             console.log("error", error);
                                         })     
                    })
                    .catch( error => {
                       console.log("error",error);
                        return error;
                    })

    },
     authenticateJspAdmin: (siteId, jspAdminUrl,userName, passWord) => {

       return new Promise( async (resolve, reject) => {

            try{

                const browser = await puppeteer.launch({headless: false})
                const page = await browser.newPage();
            
                await page.goto(jspAdminUrl);
            
                await page.waitForSelector('.button1')
            
                await page.type("input[name=user]", userName);
            
                await page.type("input[name=pass]", passWord); 
                await page.click('.button1');
            
                await page.waitForSelector('.lgbutton');
            
                await page.type("input[name=reason]", "Working with MAVEN");
                await page.click('.lgbutton');
            
                await page.waitForSelector('.listheader');

                // browser.close();

                console.log("Granting LPS JspAdmin - siteId: "+siteId);

                return resolve("success")

            }catch(error){
                reject(error)
            }
       })
     },

     getAgentManagerAndAgentProfileId: (profiles, siteId, readWriteDomain) => {

        let rolesPromise = [];
        let profilesWithRoles = [];

        profiles.forEach(profile => {
            if(!profile.deleted)
                rolesPromise.push([profile.id,axios.get(uHelper.getLPProfileUrl(readWriteDomain, siteId, profile.id),{headers: headers})])
                
        })

        return Promise.all(rolesPromise)
                    .then(roles => {
                        roles.forEach(role => {
                            role[1].then(response => {
                                profilesWithRoles.push({profileId: role[0], roleName: response.data.name});       
                            })
                           
                        })
                        console.log(roles);
                    })
                    .then( () => {
                        
                        return profilesWithRoles;
                    })
                    .catch( error => {
                        console.log(error);
                    })

   
     },

     
}

module.exports = {
    LivePersonHelper
}